# Car Search Challenge

## About this project

Car Search Challenge

## Contents

- [Getting Started](#getting-started)

## Getting Started

These instructions will get a copy of the project up and running on your local machine for development and testing purposes.

```bash
# Clone the project to your machine
git clone https://gitlab.com/iamxander/car-search-challenge.git

# Navigate to the project folder
cd car-search-challenge

# Run the install
npm install

# Start the development server
npm start
```
