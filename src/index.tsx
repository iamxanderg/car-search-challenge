import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Homepage from "./components/pages/homepage";
import SignInPage from "./components/pages/signin-page";

ReactDOM.render(
  <Router>
    <Route exact path="/signin" component={SignInPage} />
    <Route exact path="/" component={Homepage} />
  </Router>,
  document.getElementById("root")
);
