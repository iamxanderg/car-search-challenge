import * as React from "react";
import { Component } from "react";
import HeaderBar from "../../molecules/header-bar";
import Text from "../../atoms/text";
import Card from "../../molecules/card";
import SearchPanel from "../../organisms/search-panel";
import "./styles.scss";

export default class Homepage extends Component {
  handleFindCar() {
    console.log("Find Car Clicked");
  }

  handleTabs() {
    console.log("Tab Clicked");
  }

  render() {
    return (
      <div className="page">
        <div className="homepage">
          <HeaderBar />
          <section className="page__section">
            <div className="searchSection">
              <div className="searchSection__title">
                <Text
                  type="heading"
                  text="Here to help find your perfect car"
                  classNames="searchSection__title--shadow"
                />
                <Text
                  type="subheading"
                  text="Find and buy your next car from the comfort of your own home"
                />
              </div>
              <SearchPanel
                onTabClick={() => this.handleTabs()}
                onFindCar={() => this.handleFindCar()}
                tabClasses="searchSection__tabs"
              />
            </div>
          </section>
          <section className="page__section">
            <div className="cardSection">
              <Card
                icon="car"
                title="Find a perfect match"
                fill="#f18f01"
                classNames="cardSection__title cardSection__title--car"
              >
                <Text
                  type="paragraph"
                  text="We help find the perfect car for your needs and wishes"
                  classNames=""
                />
              </Card>
              <Card
                icon="config"
                title="Configure your dream vehicle"
                fill="#c73e1d"
                classNames="cardSection__title cardSection__title--config"
              >
                <Text
                  type="paragraph"
                  text="Filter and sort to browse the vehicle of your dreams"
                  classNames=""
                />
              </Card>
              <Card
                icon="secure"
                title="Online, secure checkout"
                fill="#a23b72"
                classNames="cardSection__title cardSection__title--secure"
              >
                <Text
                  type="paragraph"
                  text="Buy your vehicle directly online in our safe &amp; secure checkout environment"
                  classNames=""
                />
              </Card>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
