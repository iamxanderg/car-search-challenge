import * as React from "react";
import { Component } from "react";
import HeaderBar from "../../molecules/header-bar";
import SignIn from "../../organisms/sign-in";
import "./styles.scss";

export default class SignInPage extends Component {
  handleSignIn() {
    console.log("Sign In Clicked");
  }

  render() {
    return (
      <div className="page">
        <div className="signinpage">
          <HeaderBar />

          <SignIn onSignInClick={() => this.handleSignIn()} />
        </div>
      </div>
    );
  }
}
