import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Icon, { IProps } from "./Icon";

export default {
  title: "Atoms/Icon",
  component: Icon,
  argTypes: {
    icon: { control: "select" },
    height: { control: "text" },
    width: { control: "text" },
    viewBoxSize: { control: "text" },
    fill: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <Icon {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  icon: "car",
  height: "24",
  width: "24",
  viewBoxSize: "0 0 32 32",
};
