import * as React from "react";
import { v4 } from "uuid";
import { IconNames, renderSVG } from "./Icons";

export interface IProps {
  icon: IconNames;
  height?: string;
  width?: string;
  viewBoxSize?: string;
  fill?: string;
}

const Icon = ({
  icon,
  height = "24",
  width = "24",
  viewBoxSize = "0 0 32 32",
  fill = "#c5c5c5",
}: IProps) => {
  return (
    <>
      <svg
        viewBox={viewBoxSize}
        height={height}
        width={width}
        role="presentation"
        preserveAspectRatio="xMidYMid meet"
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        fill={fill}
        stroke={fill}
      >
        <g id={v4()}>{renderSVG(icon)}</g>
      </svg>
    </>
  );
};

export default Icon;
