import * as React from "react";
import { useEffect, useRef, useCallback } from "react";
import { ListItem } from "./models/ListItem";
import "./styles.scss";

export interface IProps {
  item: ListItem;
  focus: any;
  index: number;
  setFocus: Function;
  onClick: (title: string) => void;
}

const DropdownItem = ({ item, onClick, focus, index, setFocus }: IProps) => {
  const { id, title } = item;
  const ref = useRef(null);

  useEffect(() => {
    if (focus) {
      ref.current.focus();
    }
  }, [focus]);

  const handleSelect = useCallback(() => {
    onClick(title);
    setFocus(index);
  }, [title, index, setFocus]);

  return (
    <li
      key={id}
      tabIndex={focus ? 0 : -1}
      ref={ref}
      onClick={handleSelect}
      onKeyPress={handleSelect}
      className="listItem"
    >
      {title}
    </li>
  );
};

export default DropdownItem;
