import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Dropdown, { IProps } from "./Dropdown";

export default {
  title: "Atoms/Dropdown",
  component: Dropdown,
  argTypes: {
    items: { control: "object" },
    placeholder: { control: "text" },
    disabled: { control: "boolean" },
    classNames: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <Dropdown {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  items: [
    { id: "1", title: "Item 1" },
    { id: "2", title: "Item 2" },
    { id: "3", title: "Item 3" },
  ],
};
