import * as React from "react";
import { useState, useRef, useEffect } from "react";
import cn from "classnames";
import { ListItem } from "./models/ListItem";
import useFocus from "./hooks/useFocus";
import DropdownItem from "./DropdownItem";
import "./styles.scss";

export interface IProps {
  items: ListItem[];
  placeholder?: string;
  disabled?: boolean;
  classNames?: string;
}

const Dropdown = ({
  items,
  placeholder = "Select",
  disabled = false,
  classNames,
}: IProps) => {
  const [selected, setSelected] = useState(placeholder);
  const [focus, setFocus] = useFocus(items.length);
  const [isOpen, setOpen] = useState(false);
  const wrapperRef = useRef();
  const selectedRef = useRef();

  useEffect(() => {
    document.addEventListener("mousedown", handleOnClickOutside);
    document.addEventListener("keydown", handleOnKeypress);

    return () => {
      document.removeEventListener("mousedown", handleOnClickOutside);
      document.removeEventListener("keydown", handleOnKeypress);
    };
  }, []);

  const handleOnClickOutside = (event: any) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setOpen(false);
    }
  };

  const handleOnKeypress = (event: any) => {
    if (disabled) return;

    switch (event.keyCode) {
      case 13:
        if (selectedRef.current.contains(event.target) && !isOpen) {
          setOpen(true);
        }
        break;
      case 27:
        setOpen(false);
        break;
      default:
        break;
    }
  };

  const toggleOpen = () => {
    if (disabled) return;

    setOpen((isOpen) => !isOpen);
  };

  const onItemClick = (value: string) => {
    if (disabled) return;

    setSelected(value);
    setOpen(false);
  };

  return (
    <div ref={wrapperRef} className={cn("wrapper", { ["isOpen"]: isOpen })}>
      <div
        ref={selectedRef}
        tabIndex={0}
        onClick={toggleOpen}
        className={cn("header", classNames, { ["isDisabled"]: disabled })}
      >
        <div
          className={cn(
            "selected",
            {
              ["isPlaceholder"]: selected === placeholder,
            },
            { ["isDisabled"]: disabled }
          )}
        >
          {selected}
        </div>
        <div className={cn("toggle", { ["isDisabled"]: disabled })} />
      </div>
      <ul className="list">
        {items.map((item, index) => (
          <DropdownItem
            key={item.id}
            setFocus={setFocus}
            index={index}
            focus={focus === index}
            item={item}
            onClick={onItemClick}
          />
        ))}
      </ul>
    </div>
  );
};

export default Dropdown;
