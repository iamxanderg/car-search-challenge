import React from "react";
import { shallow } from "enzyme";
import Dropdown from "../Dropdown";

describe("Dropdown", () => {
  const placeholderText = "Select a dropdown option";
  const itemsArray = [
    { title: "item 1", id: "1" },
    { title: "item 2", id: "2" },
    { title: "item 3", id: "3" },
    { title: "item 4", id: "4" },
  ];

  it("placeholder matches text", () => {
    const wrapper = shallow(
      <Dropdown placeholder={placeholderText} items={itemsArray} />
    );
    const hasText = wrapper.find("div.dd-selected").text();

    expect(hasText).toEqual(placeholderText);
  });

  it("placeholder does not match text", () => {
    const wrapper = shallow(
      <Dropdown placeholder={placeholderText} items={itemsArray} />
    );
    const hasText = wrapper.find("div.dd-selected").text();

    expect(hasText).not.toEqual("placeholder text");
  });
});
