# Dropdown

The `Dropdown` component allows one option to be selected from a list.

## Import

To import into `.jsx` file use:

```js
import Dropdown from "./dropdown/";
```

## SCSS

To style the component import the following stylesheet:

```scss
import "./dropdown.scss";
```

# SCSS Dependencies

The SCSS style classes for this component are dependent on the SCSS utilities library.

## Props

This component supports the following props:

- identifier:

  - Type: `string`
  - Required: **Yes**
  - Usage: Unique Identifier

- placeholder:

  - Type: `string`
  - Required: **No**
  - Usage: Help text to display when component is empty

- disabled:

  - Type: `boolean`
  - Required: **No**
  - Usage: Sets the component as disabled

- items:

  - Type: `array`
  - Required: **Yes**
  - Usage: List of options for component
