import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Text, { IProps } from "./Text";

export default {
  title: "Atoms/Text",
  component: Text,
  argTypes: {
    text: { control: "text" },
    type: { control: "select" },
    classNames: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <Text {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  type: "label",
  text: "Sign In",
};
