import * as React from "react";
import cn from "classnames";
import "./styles.scss";

const TYPES = {
  label: "label",
  paragraph: "paragraph",
  sectionLabel: "sectionLabel",
  sectionTitle: "sectionTitle",
  subheading: "subheading",
  heading: "heading",
};
export type TextTypes = keyof typeof TYPES;

export interface IProps {
  text: string;
  type: TextTypes;
  classNames?: string;
}

const Text = ({ text, type, classNames }: IProps) => {
  return <p className={cn("text", type, classNames)}>{text}</p>;
};

export default Text;
