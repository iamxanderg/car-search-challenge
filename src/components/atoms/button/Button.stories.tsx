import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Button, { IProps } from "./Button";

export default {
  title: "Atoms/Button",
  component: Button,
  argTypes: {
    ariaLabel: { contrrol: "text" },
    classNames: { contrrol: "text" },
    children: { control: "reactnode" },
    onClick: { action: "onClick" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <Button {...args}>Button</Button>;

export const Rendered = Template.bind({});
