import * as React from "react";
import { ReactNode } from "react";
import cn from "classnames";
import "./styles.scss";

export interface IProps {
  ariaLabel: string
  classNames?: string;
  children?: ReactNode;
  onClick?: () => void;
}

const Button = ({ ariaLabel, classNames, children, onClick }: IProps) => {
  return (
    <>
      <button
        type="button"
        aria-label={ariaLabel}
        onClick={() => {onClick ? onClick() : null}}
        className={cn("button", classNames)}
      >
        {children}
      </button>
    </>
  );
};

export default Button;
