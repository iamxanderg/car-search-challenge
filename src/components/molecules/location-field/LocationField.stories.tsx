import React from "react";
import { v4 } from "uuid";
import { Story, Meta } from "@storybook/react/types-6-0";
import LocationField, { IProps } from "./LocationField";

export default {
  title: "Molecules/LocationField",
  component: LocationField,
  argTypes: {
    items: { control: "object" },
    label: { control: "text" },
    placeholder: { control: "text" },
    classNames: { control: "text" },
    labelClasses: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <LocationField {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  items: [
    { id: v4(), title: "5 miles" },
    { id: v4(), title: "10 miles" },
  ],
  label: "Location",
};
