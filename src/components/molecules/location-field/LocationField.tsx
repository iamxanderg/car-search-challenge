import * as React from "react";
import cn from "classnames";
import Dropdown from "../../atoms/dropdown";
import { ListItem } from "../../atoms/dropdown/models/ListItem";
import Text from "../../atoms/text";
import "./styles.scss";

export interface IProps {
  items: ListItem[];
  label?: string;
  placeholder?: string;
  classNames?: string;
  labelClasses?: string;
}

const LocationField = ({
  items,
  label,
  placeholder,
  classNames,
  labelClasses,
}: IProps) => {
  return (
    <div className="location">
      <Text type="label" text={label} classNames={labelClasses} />
      <div className="location__field">
        <input
          type="text"
          name={name}
          placeholder={placeholder}
          className={cn(
            "location__input location__input--defaults",
            classNames
          )}
          aria-label={label}
        />
        <Dropdown items={items} classNames="location__dropdown" />
      </div>
    </div>
  );
};

export default LocationField;
