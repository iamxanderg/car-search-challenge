import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import InputField, { IProps } from "./InputField";

export default {
  title: "Molecules/InputField",
  component: InputField,
  argTypes: {
    type: { control: "text" },
    name: { control: "text" },
    label: { control: "text" },
    placeholder: { control: "text" },
    labelClasses: { control: "text" },
    classNames: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <InputField {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  name: "username-field",
  label: "Username",
  placeholder: "placeholder",
};
