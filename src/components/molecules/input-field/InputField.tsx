import * as React from "react";
import cn from "classnames";
import Text from "../../atoms/text";
import "./styles.scss";

export interface IProps {
  type?: string;
  name?: string;
  label?: string;
  placeholder?: string;
  labelClasses?: string;
  classNames?: string;
}

const InputField = ({ type = "text", name, label, placeholder, labelClasses, classNames }: IProps) => {
  const cssClassNames = classNames ? classNames : "text__input--defaults";

  return (
    <div className="text">
      <Text type="label" text={label} classNames={labelClasses} />
      <input
        type={type}
        name={name}
        placeholder={placeholder}
        className={cn("text__input", cssClassNames)}
        aria-label={label}
      />
    </div>
  );
};

export default InputField;
