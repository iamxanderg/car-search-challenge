import React from "react";
import { v4 } from "uuid";
import { Story, Meta } from "@storybook/react/types-6-0";
import DropdownField, { IProps } from "./DropdownField";

export default {
  title: "Molecules/DropdownField",
  component: DropdownField,
  argTypes: {
    items: { control: "object" },
    label: { control: "text" },
    placeholder: { control: "text" },
    classNames: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <DropdownField {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  items: [
    { id: v4(), title: "5 miles" },
    { id: v4(), title: "10 miles" },
    { id: v4(), title: "15 miles" },
    { id: v4(), title: "20 miles" },
    { id: v4(), title: "25 miles" },
    { id: v4(), title: "30 miles" },
    { id: v4(), title: "35 miles" },
    { id: v4(), title: "40 miles" },
  ],
  label: "make",
  placeholder: "Any",
};
