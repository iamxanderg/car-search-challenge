import * as React from "react";
import Dropdown from "../../atoms/dropdown";
import { ListItem } from "../../atoms/dropdown/models/ListItem";
import Text from "../../atoms/text";
import "./styles.scss";

export interface IProps {
  items: ListItem[];
  label?: string;
  placeholder?: string;
  classNames?: string;
  labelClasses?: string;
}

const DropdownField = ({
  items,
  label,
  placeholder,
  classNames,
  labelClasses,
}: IProps) => {
  return (
    <div className="field">
      <Text type="label" text={label} classNames={labelClasses} />
      <Dropdown
        items={items}
        placeholder={placeholder}
        classNames={classNames}
      />
    </div>
  );
};

export default DropdownField;
