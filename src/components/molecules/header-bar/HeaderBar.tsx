import * as React from "react";
import cn from "classnames";
import { Link, useLocation } from "react-router-dom";
import Icon from "../../atoms/icon";
import Text from "../../atoms/text";
import Button from "../../atoms/button";
import "./styles.scss";

const HeaderBar = () => {
  const location = useLocation();

  return (
    <div className="headerBar">
      <Text type="sectionTitle" text="logo" classNames="headerBar__text" />

      <div className={cn("headerBar__signin", {["headerBar__signin--hide"]: location.pathname === "/signin"})}>
        <Text type="sectionTitle" text="Sign In" classNames="headerBar__text" />
        <Link to="/signin">
          <Button ariaLabel="Sign In" classNames="headerBar__button">
            <Icon
              icon="sign-in"
              height="10px"
              width="10px"
              viewBoxSize="0 0 10 10"
              fill="#2e86ab"
            />
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default HeaderBar;
