import * as React from "react";
import { ReactNode } from "react";
import Icon from "../../atoms/icon";
import { IconNames } from "../../atoms/icon/Icons";
import Text from "../../atoms/text";
import "./styles.scss";

export interface IProps {
  icon?: IconNames;
  title: string;
  fill?: string;
  classNames?: string;
  children?: ReactNode;
}

const Card = ({ icon, title, fill, classNames, children }: IProps) => {
  return (
    <div className="card">
      <Icon icon={icon} fill={fill} />
      <Text type="section-title" text={title} classNames={classNames} />
      {children}
    </div>
  );
};

export default Card;
