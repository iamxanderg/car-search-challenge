import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Card, { IProps } from "./Card";
import Text from "../../atoms/text";

export default {
  title: "Molecules/Card",
  component: Card,
  argTypes: {
    icon: { control: "select" },
    title: { control: "text" },
    fill: { control: "color" },
    classNames: { control: "text" },
  },
} as Meta;

const Template: Story<IProps> = (args) => (
  <Card {...args}>
    <Text type="paragraph" text="This is a Card component." />
  </Card>
);

export const Rendered = Template.bind({});
Rendered.args = {
  icon: "config",
  title: "Configure Card Panel",
};
