import * as React from "react";
import Icon from "../../atoms/icon";
import Button from "../../atoms/button";
import Text from "../../atoms/text";
import { ListItem } from "../../atoms/dropdown/models/ListItem";
import LocationField from "../../molecules/location-field";
import DropdownField from "../../molecules/dropdown-field";
import "./styles.scss";

export interface IProps {
  mileItems: ListItem[];
  makeItems: ListItem[];
  modelItems: ListItem[];
  buttonLabel: string;
  settingsLabel: string;
  onClick: () => void;
}

const CarSearch = ({
  mileItems,
  makeItems,
  modelItems,
  buttonLabel,
  settingsLabel,
  onClick,
}: IProps) => {
  return (
    <div className="carSearch">
      <LocationField
        label="Location"
        items={mileItems}
        labelClasses="carSearch__label--blue"
      />
      <div className="carSearch__details">
        <div className="dd-wrapper">
          <DropdownField
            label="make"
            items={makeItems}
            labelClasses="carSearch__label--blue"
          />
        </div>
        <DropdownField
          label="model"
          items={modelItems}
          labelClasses="carSearch__label--blue"
        />
      </div>
      <div className="carSearch__section">
        <Icon
          icon="settings"
          height="11px"
          width="12px"
          viewBoxSize="0 0 12 11"
          fill=""
        />
        <Text
          type="sectionLabel"
          text={settingsLabel}
          classNames="carSearch__label carSearch__label--blue"
        />
      </div>
      <hr className="divider" />
      <Button ariaLabel={buttonLabel} onClick={() => onClick()} classNames="primaryButton">
        <Text
          type="sectionLabel"
          text={buttonLabel}
          classNames="primaryButton__label"
        />
      </Button>
    </div>
  );
};

export default CarSearch;
