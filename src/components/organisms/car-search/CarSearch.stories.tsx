import React from "react";
import { v4 } from "uuid";
import { Story, Meta } from "@storybook/react/types-6-0";
import CarSearch, { IProps } from "./CarSearch";

export default {
  title: "Organisms/CarSearch",
  component: CarSearch,
  argTypes: {
    mileItems: { control: "object" },
    makeItems: { control: "object" },
    modelItems: { control: "object" },
    buttonLabel: { control: "text " },
    settingsLabel: { control: "text " },
    onClick: { action: "onClick" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <CarSearch {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  mileItems: [
    { id: v4(), title: "5 miles" },
  ],
  makeItems: [
    { id: v4(), title: "Audi" },
  ],
  modelItems: [
    { id: v4(), title: "A3" },
  ],
  buttonLabel: "find my car",
  settingsLabel: "set a budget",
};
