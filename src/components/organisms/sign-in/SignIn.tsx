import * as React from "react";
import { Link } from "react-router-dom";
import Button from "../../atoms/button";
import Text from "../../atoms/text";
import InputField from "../../molecules/input-field";
import "./styles.scss";

export interface IProps {
  onSignInClick: () => void;
}

const SignIn = ({ onSignInClick }: IProps) => {
  return (
    <div className="signin">
      <Text type="heading" text="Sign in" classNames="signin__heading" />

      <InputField name="username" label="Email" placeholder="Enter your email" labelClasses="signin__label--blue" />
      <Text type="label" text="Forgot email?" classNames="signin__label signin__label--blue" />

      <InputField type="password" name="password" label="Password" placeholder="Enter you password" labelClasses="signin__label--blue" />
      <Text type="label" text="Forgot password?" classNames="signin__label signin__label--blue" />

      <Link to="/" onClick={() => onSignInClick()}>
        <Button ariaLabel="Sign In"  classNames="primaryButton">
          <Text
            type="sectionLabel"
            text="Sign In"
            classNames="primaryButton__label"
          />
        </Button>
      </Link>
    </div>
  );
};

export default SignIn;
