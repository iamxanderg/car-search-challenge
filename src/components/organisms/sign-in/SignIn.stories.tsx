import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Story, Meta } from "@storybook/react/types-6-0";
import SignIn, { IProps } from "./SignIn";

export default {
  title: "Organisms/SignIn",
  component: SignIn,
  argTypes: {
    onSignInClick: { action: "onClick" },
  },
} as Meta;

const Template: Story<IProps> = (args) => <Router><SignIn {...args} /></Router>;

export const Rendered = Template.bind({});
