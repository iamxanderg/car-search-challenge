import * as React from "react";
import cn from "classnames";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Text from "../../atoms/text";
import CarSearch from "../car-search";
import "./styles.scss";

export interface IProps {
  onTabClick: () => void;
  onFindCar: () => void;
  tabClasses?: string
}

const SearchPanel = ({ onTabClick, onFindCar, tabClasses }: IProps) => {
  return (
    <Tabs selectedIndex={0} onSelect={() => onTabClick()}>
      <TabList>
        <Tab className={cn("react-tabs__tab")}>
          <Text type="sectionTitle" text="Find a car" classNames={tabClasses} />
        </Tab>
        <Tab className={cn("react-tabs__tab")}>
          <Text type="sectionTitle" text="Value my car" />
        </Tab>
      </TabList>
      <TabPanel>
        <CarSearch
          mileItems={[{id: "1", title: "40 miles"}]}
          makeItems={[{id: "1", title: "Audi"}]}
          modelItems={[{id: "1", title: "A3"}]}
          buttonLabel="find my car"
          settingsLabel="set a budget"
          onClick={() => onFindCar()}
        />
      </TabPanel>
      <TabPanel></TabPanel>
    </Tabs>
  );
};

export default SearchPanel;
