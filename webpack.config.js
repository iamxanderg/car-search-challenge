const paths = require("./paths");
// const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { presets, plugins } = require("./_webpack/webpack.config.babel");
// const copy = require("./_webpack/webpack.config.copy");

module.exports = {
  mode: "development",
  entry: paths.fromRoot("src/index.tsx"),
  devtool: "source-map",
  output: {
    filename: "bundle.js",
    path: paths.fromRoot("dist"),
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)?$/,
        use: {
          loader: "babel-loader",
          options: {
            babelrc: false,
            presets: presets,
            plugins: plugins,
          },
        },
        exclude: /node_modules/,
      },
      {
        test: /\.(scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: "css-loader" },
          { loader: "sass-loader", options: { sourceMap: true } },
        ],
      },
      { test: /\.(png|jpe?g|gif)$/i, use: [{ loader: "url-loader" }] },
      { test: /\.html$/, use: [{ loader: "html-loader" }] },
      { test: /\.svg$/, use: ["@svgr/webpack"] },
    ],
  },
  plugins: [
    // new CopyWebpackPlugin({ patterns: [...copy] }),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
    }),
    new HtmlWebpackPlugin({
      template: paths.fromRoot("src/index.html"),
      filename: "./index.html",
      inject: "body",
    }),
  ],
  devServer: {
    contentBase: paths.fromRoot("dist"),
    hot: true,
    open: true,
  },
};
