const paths = require("../paths");

module.exports = [
  {
    context: paths.fromRoot("src/assets/images"),
    from: "**/*",
    to: "assets/images",
  },
];
